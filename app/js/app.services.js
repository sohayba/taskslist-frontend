(function(){

    app.factory('categoriesService',categoriesService);

    categoriesService.$inject = [
        '_http'
    ];

    function categoriesService(_http){
        var subRoute = "/categories";

        var service = {
            create : create,
            update : update,
            delete : remove,
            view : view
        };

        function create(params){
            return _http.put(subRoute,params);
        }

        function update(params){
            return _http.post(subRoute,params);
        }

        function remove(id){
            return _http.delete(subRoute + "/" + id);
        }

        function view(id){
            if(id){
                return _http.get(subRoute + "/" + id);
            }else{
                return _http.get(subRoute);
            }
        }

        return service;
    }

})();
(function(){

    app.factory('tasksService',tasksService);

    tasksService.$inject = [
        '_http'
    ];

    function tasksService(_http){
        var subRoute = "/categories/tasks";

        var service = {
            create : create,
            update : update,
            delete : Delete,
            view : view
        };

        function create(params){
            return _http.put(subRoute,params);
        }

        function update(params){
            return _http.post(subRoute,params);
        }

        function Delete(id){
            return _http.delete(subRoute + "/" + id);
        }

        function view(id){
            if(id){
                return _http.get(subRoute + "/view/" + id);
            }else{
                return _http.get(subRoute);
            }
        }

        return service;
    }

})();
/*(function(){

    app.factory('_http',_http);

    _http.$inject = [
        '$http',
        ,'$q',
        '_notify'
    ];

    function _http($http,$q,_notify){
        var service = {
            put : put,
            patch : patch,
            delete : Delete,
            get : get
        };

        function put(link,params){
            return http("PUT",link,params);
        }

        function patch(link,params){
            return http("PATCH",link,params);
        }

        function Delete(link,params){
            return http("DELETE",link,params);
        }

        function get(link){
            return http("GET",link,{});
        }

        function http(type,link,data){
            var deferred = $q.defer();

            $http({method:type,url:API_URL + link,data:data})
            .then(function(response) {
                handleMessage(response);
                deferred.resolve(response.data.data);
            }).catch(function(response){
                handleMessage(response);
                deferred.reject({});
            });

            return deferred.promise;
        }

        function handleMessage(response){
            var status = response.status;
            var data = response.data;
            if(status == 200 && data.show_success_message){
                _notify.success(data.message);
            }

            if(status != 200){
                _notify.fail(data.message);
            }
        }

        return service;
    }

})();*/

(function(){

    app.factory('_http',_http);

    _http.$inject = [
        '$http',
        '$q',
        '_notify',
        '$rootScope'
    ];

    function _http($http,$q,_notify,$rootScope){
        var service = {
            put : put,
            post : post,
            delete : Delete,
            get : get
        };

        function put(link,params){
            return http("PUT",link,params);
        }

        function post(link,params){
            return http("POST",link,params);
        }

        function Delete(link,params){
            return http("DELETE",link,params);
        }

        function get(link){
            return http("GET",link,{});
        }

        function http(type,link,data){
            var deferred = $q.defer();
            $rootScope.isLoading++;
            $http({method:type,url:API_URL + link,data:data})
            .then(function(response) {
                handleMessage(response);
                deferred.resolve(response.data.data);
                $rootScope.isLoading--;
            }).catch(function(response){
                handleMessage(response);
                deferred.reject({});
                $rootScope.isLoading--;
            });

            return deferred.promise;
        }

        function handleMessage(response){
            var status = response.status;
            var data = response.data;
            if(status == 200 && data.show_success_message){
                _notify.success(data.message);
            }

            if(status != 200){
                _notify.fail(data.message);
            }
        }

        return service;
    }

})();
(function(){

    app.factory('_notify',_notify);

    _notify.$inject = [
        'notify'
    ];

    function _notify(notify){
        var service = {
            success : success,
            fail : fail
        }

        function success(msg){
            return notify({
                message : msg,
                classes : 'notify-success',
                duration : 2000
            });
        }

        function fail(msg){
            return notify({
                message : msg,
                classes : 'notify-fail',
                duration : 2000
            });
        }

        return service;
    }

})();