
var app = angular.module('toDoList',
	[
    'ngRoute',
	'ngAnimate',
	'ngTouch',
	'cgNotify', 
	'xeditable',
	'ui.bootstrap',
	'ui.bootstrap.tpls',
	'ui.bootstrap.tooltip',
	'angular.filter',
	'angularMoment',
	'oitozero.ngSweetAlert'
	]); 

var WEBSITE_URL = "http://127.0.0.1:8000";
var API_URL = WEBSITE_URL+"/api";