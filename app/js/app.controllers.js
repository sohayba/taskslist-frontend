(function(){

    app.controller('mainController',mainController);

    mainController.$inject = [
        '$scope',
        '$rootScope'
    ];

    function mainController($scope,$rootScope){
        $rootScope.isLoading = 0;
    }

})();
(function () {

    app
      .controller('categoriesCreateController',categoriesCreateController);

      categoriesCreateController.$inject = [
        '$scope',
        '$uibModalInstance',
        'categoriesService'
      ];

    function categoriesCreateController($scope,$uibModalInstance,categoriesService) {
        $scope.newCategory = {
          name : ""
        };
        
        $scope.createCategory = function(){
          var params = angular.copy($scope.newCategory);
          categoriesService.create(params).then(function(r){
            $uibModalInstance.close(r);
          }).catch(function(err){

          });
        }

        $scope.cancel = function () {
		      $uibModalInstance.dismiss('cancel');
		    };

    }

 })();

(function(){

    app
    .controller('categoriesListController',categoriesListController)
    .component('categoriesList', {
        templateUrl: '/components/categories/list/categories-list.html',
        controller: categoriesListController,
        bindings: {
        },
    });

    categoriesListController.$inject = [
        '$scope',
        'categoriesService',
        '$rootScope',
        '$uibModal',
        'SweetAlert'
    ]

    function categoriesListController($scope,categoriesService,$rootScope,$uibModal,SweetAlert) { 
        $scope.categories = [];

        var vm = this;
    	
    	vm.$onInit = function(){
            categoriesService.view().then(function(r){
                $scope.categories = r;
            }).catch(function(){
    
            });
        }

        $scope.createCategory = function(){
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/components/categories/create/categories-create.html',
                controller: 'categoriesCreateController',
                resolve: {
                  
                }
              });
  
              modalInstance.result.then(function (r) {
                $scope.categories.push(r);
              }, function () {
                
              });
        }

        $rootScope.$on('CATEGORY_UPDATE',function(event,data){
            var categories = $scope.categories;
            for(var i = 0, l = categories.length; i < l; i++){
                if(categories[i].category_id == data.category_id){
                    categories[i] = data;
                    break;
                }
            }

            $scope.categories = categories;
        });

        $rootScope.$on("CATEGORY_DELETE",function(event,data){
            var categories = $scope.categories;
            for(var i = 0, l = categories.length; i < l; i++){
                if(categories[i].category_id == data){
                    //categories[i] = data;
                    categories.splice(i,1);
                    break;
                }
            }

            $scope.categories = categories;
        });
    }
})();
(function(){

    app.controller('categoriesViewController',categoriesViewController);

    categoriesViewController.$inject = [
        '$scope',
        '$routeParams',
        'categoriesService',
        '$rootScope',
        'SweetAlert',
        '$location',
        '$uibModal',
        'tasksService'
    ];

    function categoriesViewController($scope,$routeParams,categoriesService,$rootScope,SweetAlert,$location,$uibModal,tasksService){
        var category_id = $routeParams.category_id;
        $scope.category = false;


        /// Get Categories List
        categoriesService.view(category_id).then(function(r){
            $scope.category = r;
            $scope.category.tasks.map((item) => {
                item.due_date = new Date(item.due_date);
                item.toNow =moment(item.due_date).toNow(); 
                return item;
            });
        }).catch(function(err){

        });

        //// Update Category
        $scope.updateCategory = function(){
            var params = angular.copy($scope.category);
            categoriesService.update(params).then(function(r){
                $scope.category = r;
                $rootScope.$broadcast('CATEGORY_UPDATE',r);
            }).catch(function(){

            });
        }


        //// Delete Category
        $scope.deleteCategory = function(){
            SweetAlert.swal({
                title: "Are you sure?",
                text: "This will delete the category and all tasks attached",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true}, 
             function(isConfirm){
                if(isConfirm){
                    categoriesService.delete(category_id).then(function(r){
                        $rootScope.$broadcast("CATEGORY_DELETE",r);
                        $location.path("/");
                    }).catch(function(err){
    
                    });
                } 
             });
        }


        //// Open new Task Modal
        $scope.createTask = function(){
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/components/tasks/create/tasks-create.html',
                controller: 'tasksCreateController',
                resolve: {
                  parm : {
                      mode : "CREATE",
                      category_id : category_id
                  }
                }
            });
    
            modalInstance.result.then(function (r) {
                r.due_date = new Date(r.due_date);
                r.toNow = moment(r.due_date).toNow();
                $scope.category.tasks.push(r);
            }, function () {
                
            });
        }

        $scope.updateTask = function(task){
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/components/tasks/create/tasks-create.html',
                controller: 'tasksCreateController',
                resolve: {
                  parm : {
                      mode : "UPDATE",
                      task : task
                  }
                }
            });
    
            modalInstance.result.then(function (r) {
                r.due_date = new Date(r.due_date);
                r.toNow = moment(r.due_date).toNow();
                var tasks = $scope.category.tasks;
                for(var i = 0,l = tasks.length; i< l; i++){
                    var currentTask = tasks[i];
                    if(currentTask.task_id == r.task_id){
                        tasks[i] = currentTask;
                        break;
                    }
                }
                $scope.category.tasks = tasks;

            }, function () {
                
            });
        }

        //// Delete task
        $scope.deleteTask = function(task_id,index){
            SweetAlert.swal({
                title: "Are you sure?",
                text: "This will delete this task",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true}, 
             function(isConfirm){
                if(isConfirm){
                    tasksService.delete(task_id).then(function(r){
                        $scope.category.tasks.splice(index,1);
                    }).catch(function(err){
    
                    });
                } 
             });
        }
    }

})();
(function(){

    app.controller('tasksCreateController',tasksCreateController);

    tasksCreateController.$inject = [
        '$scope',
        'tasksService',
        '$uibModalInstance',
        'parm'
    ];

    function tasksCreateController($scope,tasksService,$uibModalInstance,parm){

        var category_id,task;
        var mode = parm.mode;
        $scope.modalInfo = {
            title: "Create Task",
            btnText: "Create"
        };

        function reset(){
            $scope.task = {
                category_id : category_id,
                title : "",
                description : "",
                tags : "",
                due_date : new Date()
            };
        }

        if(mode == "CREATE"){
            category_id = parm.category_id;
            reset();
        }else{
            $scope.task = parm.task;
            $scope.modalInfo = {
                title : "Edit Task - " + parm.task.title,
                btnText : "Save"
            }
        }

        $scope.saveTask = function(){
            var params = angular.copy($scope.task);
            if(mode == "CREATE"){
                tasksService.create(params).then(function(r){
                    $uibModalInstance.close(r);
                }).catch(function(err){
    
                });
            }else{
                tasksService.update(params).then(function(r){
                    $uibModalInstance.close(r);
                }).catch(function(err){
    
                });
            }
        }

        $scope.cancel = function(){
            $uibModalInstance.dismiss("cancel");
        }        
    }

})();