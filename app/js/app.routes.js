app.config(function($routeProvider) {
    $routeProvider
    .when("/categories/:category_id", {
       templateUrl: '/components/categories/view/categories-view.html',
       controller: 'categoriesViewController'
    })
});