(function () {

    app
      .controller('categoriesCreateController',categoriesCreateController);

      categoriesCreateController.$inject = [
        '$scope',
        '$uibModalInstance',
        'categoriesService'
      ];

    function categoriesCreateController($scope,$uibModalInstance,categoriesService) {
        $scope.newCategory = {
          name : ""
        };
        
        $scope.createCategory = function(){
          var params = angular.copy($scope.newCategory);
          categoriesService.create(params).then(function(r){
            $uibModalInstance.close(r);
          }).catch(function(err){

          });
        }

        $scope.cancel = function () {
		      $uibModalInstance.dismiss('cancel');
		    };

    }

 })();
