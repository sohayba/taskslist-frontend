(function(){

    app
    .controller('categoriesListController',categoriesListController)
    .component('categoriesList', {
        templateUrl: '/components/categories/list/categories-list.html',
        controller: categoriesListController,
        bindings: {
        },
    });

    categoriesListController.$inject = [
        '$scope',
        'categoriesService',
        '$rootScope',
        '$uibModal',
        'SweetAlert'
    ]

    function categoriesListController($scope,categoriesService,$rootScope,$uibModal,SweetAlert) { 
        $scope.categories = [];

        var vm = this;
    	
    	vm.$onInit = function(){
            categoriesService.view().then(function(r){
                $scope.categories = r;
            }).catch(function(){
    
            });
        }

        $scope.createCategory = function(){
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/components/categories/create/categories-create.html',
                controller: 'categoriesCreateController',
                resolve: {
                  
                }
              });
  
              modalInstance.result.then(function (r) {
                $scope.categories.push(r);
              }, function () {
                
              });
        }

        $rootScope.$on('CATEGORY_UPDATE',function(event,data){
            var categories = $scope.categories;
            for(var i = 0, l = categories.length; i < l; i++){
                if(categories[i].category_id == data.category_id){
                    categories[i] = data;
                    break;
                }
            }

            $scope.categories = categories;
        });

        $rootScope.$on("CATEGORY_DELETE",function(event,data){
            var categories = $scope.categories;
            for(var i = 0, l = categories.length; i < l; i++){
                if(categories[i].category_id == data){
                    //categories[i] = data;
                    categories.splice(i,1);
                    break;
                }
            }

            $scope.categories = categories;
        });
    }
})();