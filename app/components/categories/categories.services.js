(function(){

    app.factory('categoriesService',categoriesService);

    categoriesService.$inject = [
        '_http'
    ];

    function categoriesService(_http){
        var subRoute = "/categories";

        var service = {
            create : create,
            update : update,
            delete : remove,
            view : view
        };

        function create(params){
            return _http.put(subRoute,params);
        }

        function update(params){
            return _http.post(subRoute,params);
        }

        function remove(id){
            return _http.delete(subRoute + "/" + id);
        }

        function view(id){
            if(id){
                return _http.get(subRoute + "/" + id);
            }else{
                return _http.get(subRoute);
            }
        }

        return service;
    }

})();