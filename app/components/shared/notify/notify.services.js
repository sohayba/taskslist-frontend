(function(){

    app.factory('_notify',_notify);

    _notify.$inject = [
        'notify'
    ];

    function _notify(notify){
        var service = {
            success : success,
            fail : fail
        }

        function success(msg){
            return notify({
                message : msg,
                classes : 'notify-success',
                duration : 2000
            });
        }

        function fail(msg){
            return notify({
                message : msg,
                classes : 'notify-fail',
                duration : 2000
            });
        }

        return service;
    }

})();