/*(function(){

    app.factory('_http',_http);

    _http.$inject = [
        '$http',
        ,'$q',
        '_notify'
    ];

    function _http($http,$q,_notify){
        var service = {
            put : put,
            patch : patch,
            delete : Delete,
            get : get
        };

        function put(link,params){
            return http("PUT",link,params);
        }

        function patch(link,params){
            return http("PATCH",link,params);
        }

        function Delete(link,params){
            return http("DELETE",link,params);
        }

        function get(link){
            return http("GET",link,{});
        }

        function http(type,link,data){
            var deferred = $q.defer();

            $http({method:type,url:API_URL + link,data:data})
            .then(function(response) {
                handleMessage(response);
                deferred.resolve(response.data.data);
            }).catch(function(response){
                handleMessage(response);
                deferred.reject({});
            });

            return deferred.promise;
        }

        function handleMessage(response){
            var status = response.status;
            var data = response.data;
            if(status == 200 && data.show_success_message){
                _notify.success(data.message);
            }

            if(status != 200){
                _notify.fail(data.message);
            }
        }

        return service;
    }

})();*/

(function(){

    app.factory('_http',_http);

    _http.$inject = [
        '$http',
        '$q',
        '_notify',
        '$rootScope'
    ];

    function _http($http,$q,_notify,$rootScope){
        var service = {
            put : put,
            post : post,
            delete : Delete,
            get : get
        };

        function put(link,params){
            return http("PUT",link,params);
        }

        function post(link,params){
            return http("POST",link,params);
        }

        function Delete(link,params){
            return http("DELETE",link,params);
        }

        function get(link){
            return http("GET",link,{});
        }

        function http(type,link,data){
            var deferred = $q.defer();
            $rootScope.isLoading++;
            $http({method:type,url:API_URL + link,data:data})
            .then(function(response) {
                handleMessage(response);
                deferred.resolve(response.data.data);
                $rootScope.isLoading--;
            }).catch(function(response){
                handleMessage(response);
                deferred.reject({});
                $rootScope.isLoading--;
            });

            return deferred.promise;
        }

        function handleMessage(response){
            var status = response.status;
            var data = response.data;
            if(status == 200 && data.show_success_message){
                _notify.success(data.message);
            }

            if(status != 200){
                _notify.fail(data.message);
            }
        }

        return service;
    }

})();