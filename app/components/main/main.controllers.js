(function(){

    app.controller('mainController',mainController);

    mainController.$inject = [
        '$scope',
        '$rootScope'
    ];

    function mainController($scope,$rootScope){
        $rootScope.isLoading = 0;
    }

})();