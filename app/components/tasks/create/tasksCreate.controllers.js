(function(){

    app.controller('tasksCreateController',tasksCreateController);

    tasksCreateController.$inject = [
        '$scope',
        'tasksService',
        '$uibModalInstance',
        'parm'
    ];

    function tasksCreateController($scope,tasksService,$uibModalInstance,parm){

        var category_id,task;
        var mode = parm.mode;
        $scope.modalInfo = {
            title: "Create Task",
            btnText: "Create"
        };

        function reset(){
            $scope.task = {
                category_id : category_id,
                title : "",
                description : "",
                tags : "",
                due_date : new Date()
            };
        }

        if(mode == "CREATE"){
            category_id = parm.category_id;
            reset();
        }else{
            $scope.task = parm.task;
            $scope.modalInfo = {
                title : "Edit Task - " + parm.task.title,
                btnText : "Save"
            }
        }

        $scope.saveTask = function(){
            var params = angular.copy($scope.task);
            if(mode == "CREATE"){
                tasksService.create(params).then(function(r){
                    $uibModalInstance.close(r);
                }).catch(function(err){
    
                });
            }else{
                tasksService.update(params).then(function(r){
                    $uibModalInstance.close(r);
                }).catch(function(err){
    
                });
            }
        }

        $scope.cancel = function(){
            $uibModalInstance.dismiss("cancel");
        }        
    }

})();