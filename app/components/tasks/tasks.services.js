(function(){

    app.factory('tasksService',tasksService);

    tasksService.$inject = [
        '_http'
    ];

    function tasksService(_http){
        var subRoute = "/categories/tasks";

        var service = {
            create : create,
            update : update,
            delete : Delete,
            view : view
        };

        function create(params){
            return _http.put(subRoute,params);
        }

        function update(params){
            return _http.post(subRoute,params);
        }

        function Delete(id){
            return _http.delete(subRoute + "/" + id);
        }

        function view(id){
            if(id){
                return _http.get(subRoute + "/view/" + id);
            }else{
                return _http.get(subRoute);
            }
        }

        return service;
    }

})();