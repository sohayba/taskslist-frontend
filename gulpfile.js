var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var gulpIf = require('gulp-if');
var wrap = require('gulp-wrap');    
var argv = require('yargs').argv;
var gutil = require('gulp-util');
var file = require('gulp-file');
var watch = require('gulp-watch');
var insert = require('gulp-insert');
var injectfile = require("gulp-inject-file");
var minify = require('gulp-minify');
var version = require('gulp-version-number');


var _config = {
  hosts : {
    p  : 'http://taskslist.devserver.com', //production
    v  : 'http://localhost:8000',//'http://127.0.0.1:8000', //virtualhost
  },
  env_name : {
    p : 'PRODUCTION', //production
    l : 'LOCAL', // local
  }

}



gulp.task('browserSync', function() {
  browserSync.init({
    server: {
       baseDir: "app", 
        routes: {
        "/bower_components": "bower_components",
        "/node_modules": "node_modules"
        }
    },
        notify: {
            styles: {
                top: 'auto',
                bottom: '10px',
                right: '10px',
                borderRadius : '10px'
            }
        }
   
  })
})

gulp.task('serve-dist', function() {
  browserSync.init({
    server: {
       baseDir: "dist", 
    }
  })
})

gulp.task('html-to-ngTemplate', function() {

  return gulp
  .src(['!app/index.html','app/**/*.html'])
  .pipe(insert.transform(function(contents, file) {
      var tmp = file.path.split("app/competents/");

 
        var filePath = '/competents/'+tmp[1];
        //var filePath = filePath.split('/').join('--');
        var pre = '<!-- '+file.path+' -->\n<script type="text/ng-template" id="'+filePath+'">\n';
        var post = '\n</script>'
        return pre + contents + post;        
 
     

  }))
  .pipe(concat('app.views.html'))
  .pipe(gulp.dest('dist/'));


});


gulp.task('inject-ngTemplate', function() {
 return gulp
   .src('dist/index.html')       
   .pipe(injectfile({
        pattern: '<!--\\s*inject:<filename>-->'
    }))
   .pipe(gulp.dest('dist/'))
})


// gulp.task('copy-views', function() {
//   return gulp.src('app/**/*.html')
//   .pipe(gulp.dest('dist/'))
// })

gulp.task('copy-fcm', function() {
  gulp.src('app/js/app.fcm.js')
  .pipe(gulp.dest('dist/js'))

   gulp.src('app/firebase-messaging-sw.js')
    .pipe(gulp.dest('dist/'))
})




gulp.task('set-env', function() {

  var env = 'v';
  if(argv.env === undefined || argv.env=='' )
  {
     
  }else{
    env = argv.env;
  }
  var ip_address = _config.hosts[env];
  var env_name = _config.env_name[env];

  gutil.log(gutil.colors.yellow('Backend IP:' + ip_address));
  var text = 'var WEBSITE_URL = "'+ip_address+'";\nvar ENV = "'+env_name+'";';
  return file('app.env.js', text, { src: true })
      .pipe(gulp.dest('app/js/'));
})




gulp.task('copy:fonts:fontawesome', function () {
    return gulp.src(
        [
            'bower_components/font-awesome/fonts/**/*'
        ],
        {
            base: 'bower_components/font-awesome/fonts'
        }
    )
        .pipe(gulp.dest('app/fonts'));
});




function injectCss (file) {
      console.log('change in :  ' + file.path);
      return gulp.src(file.path)
        .pipe(browserSync.stream());
}


function sassTask(){
  return gulp.src(['app/**/vars.scss','app/**/!(vars)*.scss'])
    .pipe(concat('style.scss'))
    .pipe(sass())
    .pipe(gulp.dest('app/css'));
}


gulp.task('sass', function() {
   return sassTask();
});


gulp.task('compress', function (cb) {
  // the same options as described above

  return gulp.src('app/js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist/js/*.min.'))
});

gulp.task('useref', function(){


var options ={
  ext:{
            src:'-debug.js',
            min:'.js'
        },
        ignoreFiles: ['libs.min.js']
      };

  return gulp.src('app/index.html')
   .pipe(useref())
   //.pipe(gulpIf('*.js', minify(options)))
    // Minifies only if it's a CSS file
    //.pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'))
});

gulp.task('images', function(){
  return gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
  // Caching images that ran through imagemin
  .pipe(cache(imagemin({
      interlaced: true
    })))
  .pipe(gulp.dest('dist/images'))
});

gulp.task('fonts', function() {
  return gulp.src('app/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'))
})

gulp.task('clean:app.views', function() {

  return del.sync('dist/app.views.html'); 
})

gulp.task('clean:dist', function() {

  return del.sync('dist'); 
})


function concatJsFiles(FileType) {
  return gulp.src(['./app/**/*.'+FileType+'.js','!./app/**/app.'+FileType+'.js'])
    .pipe(concat('app.'+FileType+'.js'))
    //.pipe(uglify())
    .pipe(gulp.dest('./app/js/'));
}

gulp.task('concat-controllers', function() {
  return concatJsFiles('controllers');
})

gulp.task('concat-services', function() {
  return concatJsFiles('services');
})


gulp.task('watch', ['browserSync' ,'concat-controllers','concat-services'], function (){
  watch('app/**/*.scss', function(){
    sassTask();
  }); 
  // Other watchers
  watch('app/**/*.html', browserSync.reload); 
  // added exclusion for the CSS files generated from SASS
  watch('app/**/*.css',injectCss); 


  watch(['app/**/*.controllers.js','!app/**/app.controllers.js'], function () {
    runSequence('concat-controllers',browserSync.reload);
  }); 

  watch(['app/**/*.services.js','!app/**/app.services.js'], function () {
    runSequence('concat-services',browserSync.reload);
  });

  watch(['app/**/*.js','!app/**/*.services.js','!app/**/*.controllers.js','!app/**/app.services.js','!app/**/app.controllers.js',], browserSync.reload); 

});


gulp.task('build', function (callback) {

  var env = 'v';
  if(argv.env === undefined || argv.env=='' )
  {
     
  }else{
    env = argv.env;
  }

  runSequence(['set-env','clean:dist'],
    ['concat-controllers','concat-services','sass','copy-fcm'],
    ['useref', 'fonts'],
    ['html-to-ngTemplate'],
    ['inject-ngTemplate'],'clean:app.views',
    'version',
    callback
  )
})

gulp.task('default', function (callback) {
  runSequence(['set-env','sass','browserSync', 'watch'],
    callback
  )
})


gulp.task('version', function (callback) {
  gulp.src('dist/index.html')
    .pipe(version(
                    {
                      'append' : {

                            'key' : '_v',
                            'cover' : 0,
                            'to' : [
                                {
                                    'type' : 'css',
                                    'key' : 'ds1_build_v',
                                    'value' : '%DT%',
                                    'cover' : 1
                                },
                                  
                                {
                                    'type' : 'js',
                                    'key' : 'ds1_build_v',
                                    'value' : '%DT%',
                                    'cover' : 1
                                }
                            ]
                        },
                     
                        'output' : {
                            'file' : 'version.json'
                        }
                    }
))
    .pipe(gulp.dest('dist'));
})


//ps -efw | grep php | grep -v grep | awk '{print $2}' | xargs kill





function toTitleCase(str)
{
  var arr =  str.toLowerCase().split('-');
    if(arr.length == 1){
      return arr[0];
    }
    var first  = arr[0];
    var ret =  arr.slice(1).map(i => i[0].toUpperCase() + i.substring(1)).join('');
    return first+''+ret;
}


gulp.task('mkcomp', function() {
  // --name : js controler name , required .
  // --view :  html view name , optional . 

  if(argv.name === undefined || argv.name=='')
  {
     gutil.log(gutil.colors.yellow('Missing name !! :'),' use "gulp mkcomp --name=hays-tomeh" to create HaysTomehController ');
     return ;
  }
  else{

    var subFolder = false;
    //var folderName_orgnal = 
    if(argv.name.indexOf('/') !== -1)
    {
      subFolder = true;
    }


    var name = argv.name.split(' ').join('');
    var folderName_orginal = name;
    name = name.split('/').join('-');
    var uper_name = toTitleCase(name);
    var ctrl_name = uper_name+'Controller';
    var service_name = uper_name+'Service';
    var dashed_name = name.toLowerCase();
    var folderName = 'app/competents/'+folderName_orginal;
    

    var controller = "(function () {";
    controller = controller +"\n";
    controller = controller +"\n    dispatcherOneModule";
    controller = controller +"\n      .controller('"+ctrl_name+"',"+ctrl_name+");  ";
    controller = controller +"\n";
    controller = controller +"\n    "+ctrl_name+".$inject = ['$scope','"+service_name+"'];";
    controller = controller +"\n";
    controller = controller +"\n    function "+ctrl_name+"($scope,"+service_name+") {";
    controller = controller +"\n";
    controller = controller +"\n        console.log('This is From %c "+ctrl_name+"','background: #8e44ad; color: #fff');";
    controller = controller +"\n        "+service_name+".log(); ";
    controller = controller +"\n        $scope.ctrlName = '"+name+"'; ";
    controller = controller +"\n";
    controller = controller +"\n    }";
    controller = controller +"\n";
    controller = controller +"\n })();";

    var a =  file(uper_name+'.controllers.js', controller, { src: true })
      .pipe(gulp.dest(folderName));



    var service = "(function () {";
    service = service +"\n";
    service = service +"\n    dispatcherOneModule";
    service = service +"\n      .factory('"+service_name+"',"+service_name+");  ";
    service = service +"\n";
    service = service +"\n    "+service_name+".$inject = ['_http'];";
    service = service +"\n";
    service = service +"\n    function "+service_name+"(_http) {";
    service = service +"\n";
    service = service +"\n      var subAPIRoute = '/';";
    service = service +"\n      var service = {";
    service = service +"\n          log : log";
    service = service +"\n      }";
    service = service +"\n";
    service = service +"\n      function log(){";
    service = service +"\n          console.log('This is From %c "+service_name+"','background: #16a085; color: #fff');";
    service = service +"\n      }";
     service = service +"\n";
    service = service +"\n      return service;";
     service = service +"\n";
    service = service +"\n    }";
    service = service +"\n";
    service = service +"\n })();";

    var d =  file(uper_name+'.services.js', service, { src: true })
      .pipe(gulp.dest(folderName));


    var view_str = "<div class='"+dashed_name+"-tmplate'>\n\n\n\n<h1>Welcome In {{ctrlName}}</h1>\n<p>\n<b>Controller Name :</b> "+ctrl_name+"\n<br><b>Service Name:</b> "+service_name+"<br>\n<b>View Name :</b> "+dashed_name+".html\n<br><b>Style Name:</b> "+dashed_name+".scss</p>\n";
    view_str = view_str + "\n\n\n<b>Route</b><br><pre>\n .when('/"+dashed_name+"',{\n   templateUrl: '"+folderName+"/"+dashed_name+".html',\n   controller: '"+ctrl_name+"'\n})\n</pre>\n\n\n\n</div>";
    
    var b =  file(dashed_name+'.html', view_str, { src: true })
      .pipe(gulp.dest(folderName));

    var scss_str = "\n\n."+dashed_name+"-tmplate {";
     scss_str = scss_str +"\n";
     scss_str = scss_str +"\n}\n";

    var c =  file(dashed_name+'.scss', scss_str, { src: true })
      .pipe(gulp.dest(folderName));

      return (a & b & c & d);
  }


})


